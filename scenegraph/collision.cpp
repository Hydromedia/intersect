#include "collision.h"

Collision::Collision()
{

}

Collision::~Collision()
{

}

bool Collision::AABBandAABB(AABB one, AABB two)
{
    if ((one.max.x < two.min.x || one.min.x > two.max.x) &&
        (one.max.y < two.min.y || one.min.y > two.max.y) &&
        (one.max.z < two.min.z || one.min.z > two.max.z)){
        return true;
    } else {
        return false;
    }
}
