#ifndef SCENE_H
#define SCENE_H

#include <CS123Common.h>
#include "CS123SceneData.h"
#include <memory>
#include "Camera.h"

class Shape;
class Cube;
class Cone;
class Cylinder;
class Sphere;
class CS123ISceneParser;

// Structure for non-primitive scene objects
struct SceneStruct
{
   // Transformation for this object
   glm::mat4 transformation;

   // What primitive it is
   CS123ScenePrimitive primitive;
};

/**
 * @class Scene
 *
 * @brief This is the base class for all scenes. Modify this class if you want to provide
 * common functionality to all your scenes.
 */
class Scene
{
public:
    Scene(const QList<SceneStruct> &graph,
          const QList<CS123SceneLightData> &lights,
          const CS123SceneGlobalData &glo);
    Scene();
    virtual ~Scene();

    static void parse(Scene *sceneToFill, CS123ISceneParser *parser);
    static void debugMat4(glm::mat4 mat);

    QList<SceneStruct> m_sceneGraph = QList<SceneStruct>();
    QList<CS123SceneLightData> m_sceneLights = QList<CS123SceneLightData>();
    CS123SceneGlobalData m_globalData;
protected:

    //applies level of detail to the scene by setting tesselation values based on the number of shapes.
    void applyLOD(float numShapes);

    // Adds a primitive to the scene.
    virtual void addPrimitive(const CS123ScenePrimitive &scenePrimitive, const glm::mat4x4 &matrix, CS123SceneGlobalData data);

    // Adds a light to the scene.
    virtual void addLight(const CS123SceneLightData &sceneLight);

    // Sets the global data for the scene.
    virtual void setGlobal(const CS123SceneGlobalData &global);

    //The SceneGraph
    std::unique_ptr<Cube> m_cube;
    std::unique_ptr<Sphere> m_sphere;
    std::unique_ptr<Cone> m_cone;
    std::unique_ptr<Cylinder> m_cylinder;

private:
    static void parseNode(CS123SceneNode *node, glm::mat4 currentTrans, Scene *sceneToFill, CS123SceneGlobalData data);
};

#endif // SCENE_H
