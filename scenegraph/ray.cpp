#include "ray.h"

Ray::Ray()
{
}

Ray::~Ray(){

}


bool Ray::CubeChecker(float first, float second){
    if (first <= .5f && first >= -.5f &&
        second <= .5f && second >= -.5f) {
        return true;
    } else {
        return false;
    }
}

RayCollisionContainer Ray::checkAABB(Ray r, AABB aabb){
    RayCollisionContainer container;

    //Right Side
    float temp_t = (-r.P.x + aabb.max.x)/r.d.x;
    glm::vec4 col = r.P + r.d*temp_t;
    if (temp_t < container.t && CubeChecker(col.z, col.y)) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(1, 0, 0, 0);
    }

    //Left Side
    temp_t = (-r.P.x + aabb.min.x)/r.d.x;
    col = r.P + r.d*temp_t;
    if (temp_t < container.t && CubeChecker(col.z, col.y)) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(-1, 0, 0, 0);
    }

    //Top Side
    temp_t = (-r.P.y + aabb.max.y)/r.d.y;
    col = r.P + r.d*temp_t;
    if (temp_t < container.t && CubeChecker(col.x, col.z)) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, 1, 0, 0);
    }

    //Bottom Side
    temp_t = (-r.P.y + aabb.min.y)/r.d.y;
    col = r.P + r.d*temp_t;
    if (temp_t < container.t && CubeChecker(col.x, col.z)) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, -1, 0, 0);
    }

    //Front Side
    temp_t = (-r.P.z + aabb.max.z)/r.d.z;
    col = r.P + r.d*temp_t;
    if (temp_t < container.t && CubeChecker(col.x, col.y)) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, 0, 1, 0);
    }

    //Back Side
    temp_t = (-r.P.z + aabb.min.z)/r.d.z;
    col = r.P + r.d*temp_t;
    if (temp_t < container.t && CubeChecker(col.x, col.y)) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, 0, -1, 0);
    }
    return container;
}

RayCollisionContainer Ray::checkSphere(Ray r){
    RayCollisionContainer container;
    container.t = FLT_MAX;

    float A = r.d.x*r.d.x + r.d.y*r.d.y + r.d.z*r.d.z;
    float B = 2.0f*(r.P.x*r.d.x + r.P.y*r.d.y + r.P.z*r.d.z);
    float C = r.P.x*r.P.x + r.P.y*r.P.y + r.P.z*r.P.z - .25f;

    float det = B*B - 4.0f*A*C;
    if (det >= 0) {
        container.t = returnLowerQuadraticResult(det, A, B, C);
        container.intersection = true;
    } else {
        container.intersection = false;
    }

    glm::vec4 collisionPoint = r.P + container.t*r.d;
    container.objectSpaceNormal = glm::vec4(glm::normalize(glm::vec3(2*collisionPoint.x, 2*collisionPoint.y, 2*collisionPoint.z)), 0);
    return container;
}

RayCollisionContainer Ray::checkCone(Ray r){
    RayCollisionContainer container;
    container.t = FLT_MAX;

    //Check cone cap
    float temp_t = (-.5f - r.P.y)/r.d.y;
    float x = r.P.x + r.d.x*temp_t;
    float z = r.P.z + r.d.z*temp_t;
    if (x*x + z*z <= .5*.5) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, -1, 0, 0);
    }

    //Check cone body
    float A = (r.d.x*r.d.x) + (r.d.z*r.d.z) - (.25f*r.d.y*r.d.y);
    float B = (2.0f*r.P.x*r.d.x) + (2.0f*r.P.z*r.d.z) - (.5f*r.P.y*r.d.y) + (.25f*r.d.y);
    float C = (r.P.x*r.P.x) + (r.P.z*r.P.z) - (.25f*r.P.y*r.P.y) + (.25f*r.P.y) - (.0625f);

    float det = B*B - 4.0f*A*C;
    if (det >= 0) {
        float temp_t2 = returnLowerQuadraticResult(det, A, B, C);
        if (temp_t2 < container.t && glm::vec3(r.P + r.d*temp_t2).y > -.5 && glm::vec3(r.P + r.d*temp_t2).y < .5) {
            container.t = temp_t2;
            container.intersection = true;
            glm::vec4 collisionPoint = r.P + container.t*r.d;
            glm::vec3 xz = glm::vec3(collisionPoint.x, 0, collisionPoint.z);
            container.objectSpaceNormal = glm::normalize(glm::vec4(collisionPoint.x, glm::length(xz)/2.f, collisionPoint.z, 0));
        }
    }

    return container;
}

RayCollisionContainer Ray::checkCylinder(Ray r){
    RayCollisionContainer container;
    container.t = FLT_MAX;

    //Check bottom cap
    float temp_t = (-.5f - r.P.y)/r.d.y;
    float x = r.P.x + r.d.x*temp_t;
    float z = r.P.z + r.d.z*temp_t;
    if (x*x + z*z <= .5*.5) {
        container.t = temp_t;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, -1, 0, 0);
    }

    //Check top cap
    float temp_t2 = (.5f - r.P.y)/r.d.y;
    x = r.P.x + r.d.x*temp_t2;
    z = r.P.z + r.d.z*temp_t2;
    if (x*x + z*z <= .5*.5) {
        container.t = temp_t2;
        container.intersection = true;
        container.objectSpaceNormal = glm::vec4(0, 1, 0, 0);
    }

    //Check Cylinder body
    float A = (r.d.x*r.d.x) + (r.d.z*r.d.z);
    float B = (2.0f*r.P.x*r.d.x) + (2.0f*r.P.z*r.d.z);
    float C = (r.P.x*r.P.x) + (r.P.z*r.P.z) - 0.25f;

    float det = B*B - 4.0f*A*C;
    if (det >= 0) {
        float temp_t2 = returnLowerQuadraticResult(det, A, B, C);
        if (temp_t2 < container.t && glm::vec3(r.P + r.d*temp_t2).y > -.5 && glm::vec3(r.P + r.d*temp_t2).y < .5) {
            container.t = temp_t2;
            container.intersection = true;
            glm::vec4 collisionPoint = r.P + container.t*r.d;
            container.objectSpaceNormal = glm::normalize(collisionPoint - glm::vec4(0, collisionPoint.y, 0, 0));
        }
    }

    return container;
}

float Ray::returnLowerQuadraticResult(float det, float A, float B, float C)
{
    float res1 = (-B + sqrt(det))/(2.0f*A);
    float res2 = (-B - sqrt(det))/(2.0f*A);
    if(res1<res1){
        return res1;
    }else{
        return res2;
    }
}

