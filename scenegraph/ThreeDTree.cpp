#include "ThreeDTree.h"
#include "Collision.h"

ThreeDTree::ThreeDTree(QList<SceneStruct> sceneGraph) : m_scenegraph(sceneGraph)
{
    QList<ShapeResult> res = QList<ShapeResult>();
    glm::vec3 min;
    glm::vec3 max;

    for (int i = 0; i < m_scenegraph.length(); i++) {
        ShapeResult r;
        r.sceneStruct = m_scenegraph.at(i);
        r.boundingBox = getBoundingBox(m_scenegraph.at(i));
        min = min2(min, r.boundingBox.min);
        max = max2(max, r.boundingBox.max);
        res.append(r);
    }
    AABB aabb;
    aabb.max = max;
    aabb.min = min;
    m_root = constructTree(0, aabb, res);
}

node* ThreeDTree::constructTree(int depth, AABB currentBox, QList<ShapeResult> currList){
    if (currList.length() <= MAX_PRIMITIVES){
        node *leaf = new node();
        leaf->boundingBox = currentBox;
        leaf->primitives = currList;
        leaf->isLeaf = true;
        return leaf;
    } else {

       float overallMinCost = FLT_MAX;
       QList<ShapeResult> leftList;
       QList<ShapeResult> rightList;
       AABB leftBox;
       AABB rightBox;

        for (int i = 0; i < currList.length(); i++) {
            AABB bbox = currList.at(i).boundingBox;
            AABB minLeftBox = getMinLeftBox(currentBox, bbox, depth % 3);
            AABB minRightBox = getMinRightBox(currentBox, bbox, depth % 3);
            AABB maxLeftBox = getMaxLeftBox(currentBox, bbox, depth % 3);
            AABB maxRightBox = getMaxRightBox(currentBox, bbox, depth % 3);

            QList<ShapeResult> minLeftPrimitives = QList<ShapeResult>();
            QList<ShapeResult> minRightPrimitives = QList<ShapeResult>();
            QList<ShapeResult> maxLeftPrimitives = QList<ShapeResult>();
            QList<ShapeResult> maxRightPrimitives = QList<ShapeResult>();

            for (int j = 0; j < currList.length(); j++) {
                AABB jBbox = currList.at(j).boundingBox;
                Collision c  = Collision();
                if (c.AABBandAABB(jBbox, minLeftBox)) {
                    ShapeResult s;
                    s.boundingBox = minLeftBox;
                    s.sceneStruct = currList.at(j).sceneStruct;
                    minLeftPrimitives.append(s);
                }
                if (c.AABBandAABB(jBbox, minRightBox)) {
                    ShapeResult s;
                    s.boundingBox = minRightBox;
                    s.sceneStruct = currList.at(j).sceneStruct;
                    minRightPrimitives.append(s);
                }
                if (c.AABBandAABB(jBbox, maxLeftBox)) {
                    ShapeResult s;
                    s.boundingBox = maxLeftBox;
                    s.sceneStruct = currList.at(j).sceneStruct;
                    maxLeftPrimitives.append(s);
                }
                if (c.AABBandAABB(jBbox, maxRightBox)) {
                    ShapeResult s;
                    s.boundingBox = maxRightBox;
                    s.sceneStruct = currList.at(j).sceneStruct;
                    maxRightPrimitives.append(s);
                }
            }
            float min_cost = calculateCost(minLeftBox, minLeftPrimitives, minRightBox, minRightPrimitives);

            float max_cost = calculateCost(maxLeftBox, maxLeftPrimitives, maxRightBox, maxRightPrimitives);

            if (min_cost <= max_cost && min_cost < overallMinCost) {
                overallMinCost = min_cost;
                leftList = minLeftPrimitives;
                rightList = minRightPrimitives;
                leftBox = minLeftBox;
                rightBox = minRightBox;
            } else if (max_cost < overallMinCost) {
                overallMinCost = max_cost;
                leftList = maxLeftPrimitives;
                rightList = maxRightPrimitives;
                leftBox = maxLeftBox;
                rightBox = maxRightBox;
            }
        }

        node *branch = new node();
        branch->boundingBox = currentBox;
        branch->primitives = currList;
        branch->isLeaf = false;
        branch->left = constructTree(depth + 1, leftBox, leftList);
        branch->right = constructTree(depth + 1, rightBox, rightList);
        return branch;
    }
}
AABB ThreeDTree::getMinLeftBox(AABB currentBox, AABB bbox, int modResult){
    AABB minLeftBox = currentBox;
    minLeftBox.min = currentBox.min;
    minLeftBox.max = currentBox.max;
    if (modResult == 0) {
        minLeftBox.max.x = bbox.min.x;
    } else if (modResult == 1) {
        minLeftBox.max.y = bbox.min.y;
    } else if (modResult == 2) {
        minLeftBox.max.z = bbox.min.z;
    }
    return minLeftBox;
}

AABB ThreeDTree::getMinRightBox(AABB currentBox, AABB bbox, int modResult){
    AABB minRightBox = currentBox;
    minRightBox.min = currentBox.min;
    minRightBox.max = currentBox.max;
    if (modResult == 0) {
        minRightBox.min.x = bbox.min.x;
    } else if (modResult == 1) {
        minRightBox.min.y = bbox.min.y;
    } else if (modResult == 2) {
        minRightBox.min.z = bbox.min.z;
    }
    return minRightBox;
}

AABB ThreeDTree::getMaxLeftBox(AABB currentBox, AABB bbox, int modResult){
    AABB maxLeftBox = currentBox;
    maxLeftBox.min = currentBox.min;
    maxLeftBox.max = currentBox.max;
    maxLeftBox.max.x = bbox.max.x;
    if (modResult == 0) {
        maxLeftBox.max.x = bbox.max.x;
    } else if (modResult == 1) {
        maxLeftBox.max.y = bbox.max.y;
    } else if (modResult == 2) {
        maxLeftBox.max.z = bbox.max.z;
    }
    return maxLeftBox;
}

AABB ThreeDTree::getMaxRightBox(AABB currentBox, AABB bbox, int modResult){
    AABB maxRightBox = currentBox;
    maxRightBox.min = currentBox.min;
    maxRightBox.max = currentBox.max;
    maxRightBox.min.x = bbox.max.x;
    if (modResult == 0) {
       maxRightBox.min.x = bbox.max.x;
    } else if (modResult == 1) {
        maxRightBox.min.y = bbox.max.y;
    } else if (modResult == 2) {
        maxRightBox.min.z = bbox.max.z;
    }
    return maxRightBox;
}

AABB ThreeDTree::getBoundingBox(SceneStruct current) {
    AABB bbox;
    glm::vec3 backLeftTop = glm::vec3((current.transformation * glm::vec4(-.5, -.5, -.5, 1)));
    glm::vec3 backRightTop = glm::vec3((current.transformation * glm::vec4(.5, -.5, -.5, 1)));
    glm::vec3 frontLeftTop = glm::vec3((current.transformation * glm::vec4(-.5, -.5, .5, 1)));
    glm::vec3 frontRightTop = glm::vec3((current.transformation * glm::vec4(.5, -.5, .5, 1)));

    glm::vec3 backLeftBottom = glm::vec3((current.transformation * glm::vec4(-.5, .5, -.5, 1)));
    glm::vec3 backRightBottom = glm::vec3((current.transformation * glm::vec4(.5, .5, -.5, 1)));
    glm::vec3 frontLeftBottom = glm::vec3((current.transformation * glm::vec4(-.5, .5, .5, 1)));
    glm::vec3 frontRightBottom = glm::vec3((current.transformation * glm::vec4(.5, .5, .5, 1)));
    bbox.min  = min8(backLeftTop, backRightTop, frontLeftTop, frontRightTop, backLeftBottom, backRightBottom, frontLeftBottom, frontRightBottom);
    bbox.max  = max8(backLeftTop, backRightTop, frontLeftTop, frontRightTop, backLeftBottom, backRightBottom, frontLeftBottom, frontRightBottom);
    return bbox;
}

glm::vec3 ThreeDTree::min8(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, glm::vec3 e, glm::vec3 f, glm::vec3 g, glm::vec3 h){
    float x = min(min(min(a.x,b.x), min(d.x, e.x)), min(min(f.x, g.x), h.x));
    float y = min(min(min(a.y,b.y), min(d.y, e.y)), min(min(f.y, g.y), h.y));
    float z = min(min(min(a.z,b.z), min(d.z, e.z)), min(min(f.z, g.z), h.z));
    return glm::vec3(x, y, z);
}

glm::vec3 ThreeDTree::max8(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, glm::vec3 e, glm::vec3 f, glm::vec3 g, glm::vec3 h){
    float x = max(max(max(a.x,b.x), max(d.x, e.x)), max(max(f.x, g.x), h.x));
    float y = max(max(max(a.y,b.y), max(d.y, e.y)), max(max(f.y, g.y), h.y));
    float z = max(max(max(a.z,b.z), max(d.z, e.z)), max(max(f.z, g.z), h.z));
    return glm::vec3(x, y, z);
}

glm::vec3 ThreeDTree::min2(glm::vec3 a, glm::vec3 b)
{
    float x = min(a.x, b.x);
    float y = min(a.y, b.y);
    float z = min(a.z, b.z);
    return glm::vec3(x,y,z);
}

glm::vec3 ThreeDTree::max2(glm::vec3 a, glm::vec3 b)
{
    float x = max(a.x, b.x);
    float y = max(a.y, b.y);
    float z = max(a.z, b.z);
    return glm::vec3(x,y,z);
}

ThreeDTree::~ThreeDTree()
{
    
}

float ThreeDTree::calculateCost(AABB leftBox, QList<ShapeResult> leftPrimitives, AABB rightBox, QList<ShapeResult> rightPrimitives)
{
    float leftArea = 0;
    float rightArea = 0;

    for (int i = 0; i < leftPrimitives.length(); i++) {
        leftArea += getSurfaceArea(leftPrimitives.at(i).boundingBox);
    }

    for (int i = 0; i < rightPrimitives.length(); i++) {
        rightArea += getSurfaceArea(rightPrimitives.at(i).boundingBox);
    }

    return (leftArea / getSurfaceArea(leftBox)) * leftPrimitives.size() + (rightArea / getSurfaceArea(rightBox)) * rightPrimitives.size();
}

float ThreeDTree::getSurfaceArea(AABB aabb){
    return 2.f * (aabb.max.x - aabb.min.x) * (aabb.max.z - aabb.min.z) +
            2 * (aabb.max.y - aabb.min.y) * (aabb.max.z - aabb.min.z) +
            2 * (aabb.max.y - aabb.min.y) * (aabb.max.x - aabb.min.x);
}


