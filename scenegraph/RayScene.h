#ifndef RAYSCENE_H
#define RAYSCENE_H

#include <QThreadPool>
#include "Scene.h"
#include "Canvas2D.h"
#include "THREEDTREE.h"

#include "qcoreapplication.h"

#include "ray.h"

/**
 * @class RayScene
 *
 *  Students will implement this class as necessary in the Ray project.
 */

struct ObjectAndRayCollisionContainer
{
   RayCollisionContainer container;
   SceneStruct object;
   Ray model_space_ray;
};

struct SuperSampleDecisionContainer
{
    glm::vec4 result;
    bool doSample;
};

class RayScene : public Scene
{


public:

    glm::vec4 m_backgroundColor;

    RayScene(const QList<SceneStruct> &graph,
             const QList<CS123SceneLightData> &lights,
             const CS123SceneGlobalData &glo,
             Canvas2D *canvas);

    virtual ~RayScene();

    void cancelRender();
    void renderGeometry(glm::vec2 startIndex, glm::vec2 endIndex);
    void startRender(Camera *camera, int numThreads);

    static float returnLowerQuadraticResult(float det, float a, float b, float c);
    static glm::vec4 convertObjectSpaceNormalToWorldSpaceNormal(glm::mat3 M3, glm::vec4 objectSpaceNormal);
    static Ray convertRayToObjectSpace(Ray r, glm::mat4 M);

protected:

private:
    //Shape getClosestObject(Ray r, glm::mat4 transform);
    ObjectAndRayCollisionContainer castRay(Ray model_space_ray, node *n, ObjectAndRayCollisionContainer current);
    ObjectAndRayCollisionContainer CalculateIntersect(float x, float y, float x_offset, float y_offset);
    SuperSampleDecisionContainer sampleCorners(float x, float y);
    glm::vec4 calculateLighting(glm::vec4 objectAmbient, glm::vec4 objectDiffuse, glm::vec4 collisionPoint, glm::vec4 N);
    Canvas2D *m_canvas;
    Camera * m_camera;
    ThreeDTree *m_tree;
    void initializeKdTree();
    //bool m_continueRendering = false;

};

#endif // RAYSCENE_H
