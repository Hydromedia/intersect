#include "Scene.h"
#include "Camera.h"
#include "CS123SceneData.h"
#include "CS123ISceneParser.h"
#include <QtGlobal>

#include "shapes/cone.h"
#include "shapes/cube.h"
#include "shapes/sphere.h"
#include "shapes/cylinder.h"

#include <tgmath.h>



Scene::Scene(const QList<SceneStruct> &graph,
             const QList<CS123SceneLightData> &lights,
             const CS123SceneGlobalData &glo) :
    m_sceneGraph(graph),
    m_sceneLights(lights),
    m_globalData(glo)
{

}

Scene::Scene(){

}

Scene::~Scene()
{
    // Do not delete m_camera, it is owned by SupportCanvas3D
}

void Scene::parse(Scene *sceneToFill, CS123ISceneParser *parser)
{
    // TODO: load scene into sceneToFill using setGlobal(), addLight(), addPrimitive(), and
    //   finishParsing()
    glm::mat4 empty = glm::mat4();
    CS123SceneGlobalData data;
    if (parser->getGlobalData(data)){
        sceneToFill->setGlobal(data);
    }
    parseNode(parser->getRootNode(), empty, sceneToFill, data);
    for (int i = 0; i < parser->getNumLights(); i++){
        CS123SceneLightData data;
        if (parser->getLightData(i, data)){
            sceneToFill->addLight(data);
        }
    }
}

void Scene::parseNode(CS123SceneNode *node, glm::mat4 currentTrans, Scene *sceneToFill, CS123SceneGlobalData data){
    for (unsigned int i = 0; i < node->transformations.size(); i++) {
        if (node->transformations[i]->type == TRANSFORMATION_TRANSLATE) {
            currentTrans = glm::translate(currentTrans, node->transformations[i]->translate);
        } else if (node->transformations[i]->type == TRANSFORMATION_SCALE) {
            currentTrans = glm::scale(currentTrans, node->transformations[i]->scale);
        } else if (node->transformations[i]->type == TRANSFORMATION_ROTATE) {
            currentTrans = glm::rotate(currentTrans, node->transformations[i]->angle, node->transformations[i]->rotate);
        } else if (node->transformations[i]->type == TRANSFORMATION_MATRIX) {
            currentTrans = (currentTrans) * (node->transformations[i]->matrix);
        }
    }
    //std::cout << "Start Oldest" << std::endl;
    //debugMat4(currentTrans);
    //std::cout << "Start Oldest" << std::endl;

    for (unsigned int i = 0; i < node->primitives.size(); i++) {
        sceneToFill->addPrimitive(*(node->primitives[i]), currentTrans, data);
    }

    for (unsigned int i = 0; i < node->children.size(); i++) {
        parseNode(node->children[i], currentTrans, sceneToFill, data);
    }
}

void Scene::addPrimitive(const CS123ScenePrimitive &scenePrimitive, const glm::mat4x4 &matrix, CS123SceneGlobalData data)
{
    CS123ScenePrimitive usePrim(scenePrimitive);
    CS123SceneFileMap *texMap(scenePrimitive.material.textureMap);
    usePrim.material.textureMap = texMap;

//    std::cout << usePrim.material.textureMap << std::endl;
//    std::cout << usePrim.material.textureMap->filename.c_str() << std::endl;
//    std::cout << &usePrim.material.textureMap->texid << std::endl;

    usePrim.material.bumpMap = scenePrimitive.material.bumpMap;

    usePrim.material.cAmbient.r = (usePrim.material.cAmbient.r) * data.ka;
    usePrim.material.cAmbient.g = (usePrim.material.cAmbient.g) * data.ka;
    usePrim.material.cAmbient.b = (usePrim.material.cAmbient.b) * data.ka;

    usePrim.material.cDiffuse.r = (usePrim.material.cDiffuse.r) * data.kd;
    usePrim.material.cDiffuse.g = (usePrim.material.cDiffuse.g) * data.kd;
    usePrim.material.cDiffuse.b = (usePrim.material.cDiffuse.b) * data.kd;

    usePrim.material.cSpecular.r = (usePrim.material.cSpecular.r) * data.ks;
    usePrim.material.cSpecular.g = (usePrim.material.cSpecular.g) * data.ks;
    usePrim.material.cSpecular.b = (usePrim.material.cSpecular.b) * data.ks;

    SceneStruct str = {matrix, usePrim};

    m_sceneGraph.append(str);
}

void Scene::addLight(const CS123SceneLightData &sceneLight)
{
    m_sceneLights.append(sceneLight);
}

void Scene::setGlobal(const CS123SceneGlobalData &global)
{
    m_globalData = global;
}

void Scene::debugMat4(glm::mat4 mat)
{
    std::cout << mat[0][0] << ", ";
    std::cout << mat[0][1] << ", ";
    std::cout << mat[0][2] << ", ";
    std::cout << mat[0][3] << std::endl;

    std::cout << mat[1][0] << ", ";
    std::cout << mat[1][1] << ", ";
    std::cout << mat[1][2] << ", ";
    std::cout << mat[1][3] << std::endl;

    std::cout << mat[2][0] << ", ";
    std::cout << mat[2][1] << ", ";
    std::cout << mat[2][2] << ", ";
    std::cout << mat[2][3] << std::endl;

    std::cout << mat[3][0] << ", ";
    std::cout << mat[3][1] << ", ";
    std::cout << mat[3][2] << ", ";
    std::cout << mat[3][3] << std::endl;
}

void Scene::applyLOD(float numShapes)
{
    int val = std::min(log((1.f/numShapes+100.f))*(1.f/numShapes)*(1000.f), 100.0);
    std::cout << "LOD: " << val << std::endl;
    m_cube->updateBuffer(val, val, val);
    m_sphere->updateBuffer(val, val, val);
    m_cylinder->updateBuffer(val, val, val);
    m_cone->updateBuffer(val, val, val);
}
