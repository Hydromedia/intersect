#ifndef COLLISION_H
#define COLLISION_H
#include <CS123Common.h>

struct AABB {
    glm::vec3 max;
    glm::vec3 min;
};

class Collision
{
public:
    Collision();
    ~Collision();

    static bool AABBandAABB(AABB one, AABB two);


};

#endif // COLLISION_H
