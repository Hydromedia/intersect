#ifndef THREEDTREE_H
#define THREEDTREE_H
#include "Collision.h"
#include "Scene.h"
#include "Ray.h"

struct ShapeResult {
    AABB boundingBox;
    SceneStruct sceneStruct;
};

struct node {
    AABB boundingBox;
    node *left;
    node *right;
    bool isLeaf = false;
    QList<ShapeResult> primitives;
};

class ThreeDTree
{
public:
    ThreeDTree(QList<SceneStruct> sceneGraph);
    ~ThreeDTree();
    node *m_root;

private:
    const int MAX_PRIMITIVES = 3;
    QList<SceneStruct> m_scenegraph;

    node* constructTree(int depth, AABB currentBox, QList<ShapeResult> currList);

    AABB getMinLeftBox(AABB currentBox, AABB bbox, int modResult);
    AABB getMinRightBox(AABB currentBox, AABB bbox, int modResult);
    AABB getMaxLeftBox(AABB currentBox, AABB bbox, int modResult);
    AABB getMaxRightBox(AABB currentBox, AABB bbox, int modResult);

    glm::vec3 min8(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, glm::vec3 e, glm::vec3 f, glm::vec3 g, glm::vec3 h);
    glm::vec3 max8(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, glm::vec3 e, glm::vec3 f, glm::vec3 g, glm::vec3 h);
    glm::vec3 min2(glm::vec3 a, glm::vec3 b);
    glm::vec3 max2(glm::vec3 a, glm::vec3 b);
    float calculateCost(AABB leftBox, QList<ShapeResult> leftPrimitives, AABB rightBox, QList<ShapeResult> rightPrimitives);
    AABB getBoundingBox(SceneStruct current);
    float getSurfaceArea(AABB aabb);
};

#endif // THREEDTREE_H
