#include "RayRunner.h"

RayRunner::RayRunner(glm::vec2 start, glm::vec2 end, RayScene *scene)
    : m_start(start), m_end(end), m_scene(scene)
{

}

RayRunner::~RayRunner()
{

}

void RayRunner::run () {
    m_scene->renderGeometry(m_start, m_end);
}
