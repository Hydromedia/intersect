#ifndef RAY_H
#define RAY_H
#include "collision.h"

#include <CS123Common.h>

struct RayCollisionContainer
{
   glm::vec4 objectSpaceNormal;
   float t = FLT_MAX;
   bool intersection = false;
};

class Ray
{
public:
    Ray();
    ~Ray();
    glm::vec4 P;
    float t = FLT_MAX;
    glm::vec4 d;

    static bool CubeChecker(float first, float second);
    static float returnLowerQuadraticResult(float det, float A, float B, float C);
    static RayCollisionContainer checkCylinder(Ray r);
    static RayCollisionContainer checkSphere(Ray r);
    static RayCollisionContainer checkAABB(Ray r, AABB aabb);
    static RayCollisionContainer checkCone(Ray r);
};

#endif // RAY_H
