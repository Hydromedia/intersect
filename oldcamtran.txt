/**
 * @file   CamtransCamera.cpp
 *
 * This is the perspective camera class you will need to fill in for the Camtrans lab.  See the
 * lab handout for more details.
 */

#include "CamtransCamera.h"
#include <Settings.h>

CamtransCamera::CamtransCamera(float near, float far)
{
    setClip(1, 30);
    setAspectRatio(1);
    m_position = glm::vec4(2,2,2,0);
    m_look = glm::vec4(0,0,0,0);
    m_up = glm::vec4(0,1,0,0);
    setHeightAngle(0);
}

glm::mat4x4 CamtransCamera::getProjectionMatrix() const
{
    return m_projection;
}

glm::mat4x4 CamtransCamera::getViewMatrix() const
{
    return m_view;
}

glm::mat4x4 CamtransCamera::getScaleMatrix() const
{
    return m_scale;
}

glm::mat4x4 CamtransCamera::getPerspectiveMatrix() const
{
    return m_perspective;
}

glm::vec4 CamtransCamera::getPosition() const
{
    return m_position;
}

glm::vec4 CamtransCamera::getLook() const
{
    return m_look;
}

glm::vec4 CamtransCamera::getUp() const
{
    return m_up;
}

float CamtransCamera::getAspectRatio() const
{
    return m_aspectRatio;
}

float CamtransCamera::getHeightAngle() const
{
    return m_heightAngle;
}

void CamtransCamera::setAspectRatio(float a)
{
    m_aspectRatio = a;
}

void CamtransCamera::setHeightAngle(float h)
{
    m_heightAngle = h;
}

void CamtransCamera::orientLook(const glm::vec4 &eye, const glm::vec4 &look, const glm::vec4 &up)
{
    // @TODO: [CAMTRANS] Fill this in...

}

void CamtransCamera::translate(const glm::vec4 &v)
{
    // @TODO: [CAMTRANS] Fill this in...

}

void CamtransCamera::rotateU(float degrees)
{
    // @TODO: [CAMTRANS] Fill this in...

}

void CamtransCamera::rotateV(float degrees)
{
    // @TODO: [CAMTRANS] Fill this in...

}

void CamtransCamera::rotateW(float degrees)
{
    // @TODO: [CAMTRANS] Fill this in...

}

void CamtransCamera::setClip(float nearPlane, float farPlane)
{
    m_near = nearPlane, m_far = farPlane;
}

