/**
 * @file   CamtransCamera.cpp
 *
 * This obfuscated reference CamtransCamera can act as a debugging tool for Sceneview,
 * but cannot be handed in to complete the Camtrans Lab
 */

#include "CamtransCamera.h"

CamtransCamera::CamtransCamera() :
m_aspect(1), m_heightAngle(glm::radians(60.f)), m_near(1.f), m_far(30.f), m_eye(glm::vec4()), m_view(glm::mat4x4()), m_projection(glm::mat4x4())
{
    buildProjection(); orientLook(glm::vec4(2, 2, 2, 1), glm::vec4(-2, -2, -2, 0), glm::vec4(0, 1, 0, 0));
}

void CamtransCamera::buildProjection() {
    m_projection = glm::perspective(m_heightAngle, m_aspect, m_near, m_far) * 0.01f;
}

void CamtransCamera::setAspectRatio(float a)
{
    m_aspect = a;
    buildProjection();
}

glm::mat4x4 CamtransCamera::getProjectionMatrix() const
{
    return m_projection;
}

glm::mat4x4 CamtransCamera::getViewMatrix() const
{
    return m_view;
}

glm::mat4x4 CamtransCamera::getScaleMatrix() const
{
    float scale_component = m_aspect*m_far*glm::tan((m_heightAngle/2));
    glm::mat4 scale_matrix = glm::mat4(1/(scale_component), 0, 0, 0,
                                       0, m_aspect/scale_component, 0, 0,
                                       0, 0, 1/m_far, 0,
                                       0, 0, 0, 1);
    return scale_matrix;
}

glm::mat4x4 CamtransCamera::getPerspectiveMatrix() const {
    throw 0;
}

glm::vec4 CamtransCamera::getPosition() const
{
    return m_eye;
}

glm::vec4 CamtransCamera::getLook() const
{
    return glm::vec4(-m_view[0][2], -m_view[1][2], -m_view[2][2], 0.f);
}

glm::vec4 CamtransCamera::getUp() const
{
    return glm::vec4(m_view[0][1], m_view[1][1], m_view[2][1], 0.f);
}

float CamtransCamera::getAspectRatio() const
{
    return m_aspect;
}

float CamtransCamera::getHeightAngle() const
{
    return m_heightAngle;
}

void CamtransCamera::orientLook(const glm::vec4 &eye, const glm::vec4 &look, const glm::vec4 &up)
{
    m_eye = eye;
    glm::vec3 nlook = glm::normalize(glm::vec3(look));
    glm::vec3 nup = glm::normalize(glm::vec3(up));
    glm::vec3 center = glm::vec3(eye) + nlook;
    m_view = glm::lookAt(glm::vec3(eye), center, nup);
}

void CamtransCamera::setHeightAngle(float h)
{
    m_heightAngle = glm::radians(h);
    buildProjection();
}

void CamtransCamera::translate(const glm::vec4 &v)
{
    m_eye = m_eye + v;
    orientLook(m_eye, getLook(), getUp());
}

void CamtransCamera::rotateU(float degrees)
{
    glm::vec4 tempEye = m_eye;
    orientLook(glm::vec4(0.f), getLook(), getUp());
    m_view = glm::rotate(m_view, glm::radians(-degrees), glm::vec3(m_view[0][0], m_view[1][0], m_view[2][0]));
    translate(tempEye);
}

void CamtransCamera::rotateV(float degrees)
{
    glm::vec4 tempEye = m_eye; orientLook(glm::vec4(0.f), getLook(), getUp()); m_view = glm::rotate(m_view, glm::radians(-degrees), glm::vec3(getUp())); translate(tempEye);
}

void CamtransCamera::rotateW(float degrees)
{
    glm::vec4 tempEye = m_eye; orientLook(glm::vec4(0.f), getLook(), getUp()); m_view = glm::rotate(m_view, glm::radians(-degrees), glm::vec3(-getLook())); translate(tempEye);
}

void CamtransCamera::setClip(float nearPlane, float farPlane)
{
    m_near = nearPlane; m_far = farPlane <= nearPlane ? nearPlane + 0.0001 : farPlane; buildProjection();
}
