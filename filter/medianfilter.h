#ifndef MEDIANFILTER_H
#define MEDIANFILTER_H

#include "filter.h"

class MedianFilter : public Filter
{
public:
    MedianFilter();
    ~MedianFilter();
    void convolve(Canvas2D *canvas, Settings s);
    void applyFilter(Canvas2D *canvas, Settings s);
    glm::vec4 convolveHelp(glm::vec4 *array, int filterHeight, int filterWidth, glm::vec2 center, QPoint start, QPoint stop);
};

#endif // MEDIANFILTER_H
