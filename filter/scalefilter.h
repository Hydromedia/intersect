#ifndef SCALEFILTER_H
#define SCALEFILTER_H

#include "filter.h"

class ScaleFilter : public Filter
{
public:
    ScaleFilter();
    ~ScaleFilter();
    void convolve(Canvas2D *canvas, Settings s);
    void applyFilter(Canvas2D *canvas, Settings s);
    double hprime(int k, double a);
    double g(double x, double a);

private:
    glm::vec4 hprimeX(glm::vec4 *array, float a, int k, QPoint start, QPoint stop, int y);
    glm::vec4 hprimeY(glm::vec4 *array, float a, int k, QPoint start, QPoint stop, int x);
};

#endif // SCALEFILTER_H
