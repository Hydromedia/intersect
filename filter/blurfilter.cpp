#include "blurfilter.h"
#include "Canvas2D.h"

BlurFilter::BlurFilter() : Filter()
{

}

BlurFilter::~BlurFilter()
{

}

void BlurFilter::convolve(Canvas2D *canvas, Settings s)
{
    setStartAndStop(canvas);

    int size = ((m_stop.y() - m_start.y()) * (m_stop.x() - m_start.x()));
    std::cout << size << std::endl;
    glm::vec4 *start_buffer = new glm::vec4[size];

    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int ind = i*canvas->width() + j;
             start_buffer[convolutionBufferIndex] = glm::vec4(((float)canvas->data()[ind].r)/255.f,
                                                           ((float)canvas->data()[ind].g)/255.f,
                                                           ((float)canvas->data()[ind].b)/255.f,
                                                           ((float)canvas->data()[ind].a)/255.f);
             convolutionBufferIndex++;

        }
    }

    int blur_size = s.blurRadius*s.blurRadius+1;
    fillGaussian(blur_size);


    m_convolutionBuffer_1 = new glm::vec4[size];

    convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            glm::vec4 t = convolveHelp(start_buffer, canvas, m_kernel, blur_size, blur_size,
                                       glm::vec2(j - m_start.x(), i-m_start.y()), m_start, m_stop, true);
            m_convolutionBuffer_1[convolutionBufferIndex] =t;
            convolutionBufferIndex++;
        }
    }
    delete [] m_kernel;
    delete [] start_buffer;
}

void BlurFilter::applyFilter(Canvas2D *canvas, Settings s)
{

    convolve(canvas, s);

    BGRA* pix = canvas->data();
    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int pixel_index = i*(canvas->width()) + j;
            pix[pixel_index].r = (char) (m_convolutionBuffer_1[convolutionBufferIndex].x*255.f);
            pix[pixel_index].g = (char) (m_convolutionBuffer_1[convolutionBufferIndex].y*255.f);
            pix[pixel_index].b = (char) (m_convolutionBuffer_1[convolutionBufferIndex].z*255.f);
            convolutionBufferIndex++;
        }
    }

    delete [] m_convolutionBuffer_1;
}

