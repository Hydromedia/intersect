#ifndef GREYSCALEFILTER_H
#define GREYSCALEFILTER_H

#include "filter/filter.h"

class GreyscaleFilter : public Filter
{
public:
    GreyscaleFilter();
    ~GreyscaleFilter();

    void applyFilter(Canvas2D *canvas, Settings s);
};

#endif // GREYSCALEFILTER_H
