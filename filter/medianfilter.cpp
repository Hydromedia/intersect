#include "medianfilter.h"
#include "Canvas2D.h"
#include <algorithm>

MedianFilter::MedianFilter() : Filter()
{
}

MedianFilter::~MedianFilter()
{

}
void MedianFilter::convolve(Canvas2D *canvas, Settings s)
{
    setStartAndStop(canvas);

    int size = ((m_stop.y() - m_start.y()) * (m_stop.x() - m_start.x()));
    std::cout << size << std::endl;
    glm::vec4 *start_buffer = new glm::vec4[size];

    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int ind = i*canvas->width() + j;
             start_buffer[convolutionBufferIndex] = glm::vec4(((float)canvas->data()[ind].r)/255.f,
                                                           ((float)canvas->data()[ind].g)/255.f,
                                                           ((float)canvas->data()[ind].b)/255.f,
                                                           ((float)canvas->data()[ind].a)/255.f);
             convolutionBufferIndex++;

        }
    }

    int blur_size = s.blurRadius*s.blurRadius+1;
    fillGaussian(blur_size);


    m_convolutionBuffer_1 = new glm::vec4[size];

    convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            glm::vec4 t = convolveHelp(start_buffer, 3, 3, glm::vec2(j - m_start.x(), i-m_start.y()), m_start, m_stop);
            m_convolutionBuffer_1[convolutionBufferIndex] =t;
            convolutionBufferIndex++;
        }
    }
    delete [] start_buffer;
}

void MedianFilter::applyFilter(Canvas2D *canvas, Settings s)
{
    convolve(canvas, s);

    BGRA* pix = canvas->data();
    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int pixel_index = i*(canvas->width()) + j;
            pix[pixel_index].r = (char) (m_convolutionBuffer_1[convolutionBufferIndex].x*255.f);
            pix[pixel_index].g = (char) (m_convolutionBuffer_1[convolutionBufferIndex].y*255.f);
            pix[pixel_index].b = (char) (m_convolutionBuffer_1[convolutionBufferIndex].z*255.f);
            convolutionBufferIndex++;
        }
    }

    delete [] m_convolutionBuffer_1;
}

glm::vec4 MedianFilter::convolveHelp(glm::vec4 *array, int filterHeight, int filterWidth, glm::vec2 center, QPoint start, QPoint stop)
{

    int start_of_array_X = (((int)(center.x)) - (filterWidth)/2);
    int start_of_array_Y = (((int)(center.y)) - (filterHeight)/2);

    int end_of_array_X = (((int)(center.x)) + (filterWidth)/2);
    int end_of_array_Y = (((int)(center.y)) + (filterHeight)/2);

    glm::vec4 sum = glm::vec4(0,0,0,0);

    float x_array[9];
    float y_array[9];
    float z_array[9];
    for (int i = start_of_array_Y; i <= end_of_array_Y; i++) {
        for (int j = start_of_array_X; j <= end_of_array_X; j++) {
            int f_i = i - start_of_array_Y;
            int f_j = j - start_of_array_X;

            int use_i = i;
            int use_j = j;

            int filter_index = f_i*(filterWidth) + f_j;

            if (0 > start_of_array_X) {
                use_j = 0;
            } else if (stop.x()-start.x()-1 < end_of_array_X) {
                use_j = (stop.x()-start.x()-1);
            } if (0 > start_of_array_Y) {
                use_i = 0;
            } else if (stop.y()-start.y()-1 < end_of_array_Y) {
                use_i = (stop.y()-start.y()-1);
            }

            int array_index = use_i*(stop.x() - start.x()) + use_j;
            x_array[filter_index] = array[array_index].x;
            y_array[filter_index] = array[array_index].y;
            z_array[filter_index] = array[array_index].z;
        }
    }
    std::sort(x_array, x_array + 9);
    std::sort(y_array, y_array + 9);
    std::sort(z_array, z_array + 9);
    sum = glm::vec4(x_array[5], y_array[5], z_array[5], 0);
    return sum;
}

