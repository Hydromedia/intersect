#include "invertfilter.h"
#include "Canvas2D.h"

InvertFilter::InvertFilter()
    : Filter()
{

}

InvertFilter::~InvertFilter()
{

}

void InvertFilter::applyFilter(Canvas2D *canvas, Settings s)
{
    setStartAndStop(canvas);

    BGRA* pix = canvas->data();
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int pixel_index = i*(canvas->width()) + j;
            pix[pixel_index].r = 255-pix[pixel_index].r;
            pix[pixel_index].g = 255-pix[pixel_index].g;
            pix[pixel_index].b = 255-pix[pixel_index].b;
        }
    }
}

