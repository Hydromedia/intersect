#ifndef FILTER_H
#define FILTER_H
#include <CS123Common.h>
#include <algorithm>
#include "Settings.h"
#include <memory>
class Canvas2D;


class Filter
{
public:
    Filter();
    ~Filter();

    virtual void applyFilter(Canvas2D *canvas, Settings s) = 0;

protected:
    virtual glm::vec4 convolveHelp(glm::vec4 *array, Canvas2D *canvas, float *filter,
                      int filterHeight, int filterWidth, glm::vec2 center, QPoint start, QPoint stop, bool averaged);
    void setStartAndStop(Canvas2D *canvas);

    QPoint m_start;
    QPoint m_stop;

    void fillGaussian(int size);
    glm::vec4 *m_convolutionBuffer_1;
    glm::vec4 *m_convolutionBuffer_2;
    float *m_kernel;
};

#endif // FILTER_H
