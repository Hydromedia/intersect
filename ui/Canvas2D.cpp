/**
 * @file Canvas2D.cpp
 *
 * CS123 2-dimensional canvas. Contains support code necessary for Brush, Filter, Intersect, and
 * Ray.
 *
 * YOU WILL NEED TO FILL THIS IN!
 *
 */

// For your convenience, a few headers are included for you.
#include <math.h>
#include <assert.h>
#include <unistd.h>
#include "Canvas2D.h"
#include "Canvas3D.h"
#include "Settings.h"
#include "RayScene.h"


#include <QPainter>


Canvas2D::Canvas2D()
{
    // @TODO: Initialize any pointers in this class here.
    m_scene = NULL;


}

Canvas2D::~Canvas2D()
{
    // @TODO: Be sure to release all memory that you allocate.
    delete m_scene;

}

// This is called when the canvas size is changed. You can change the canvas size by calling
// resize(...). You probably won't need to fill this in, but you can if you want to.
void Canvas2D::notifySizeChanged(int w, int h) {

}

void Canvas2D::paintEvent(QPaintEvent *e) {
    // You probably won't need to fill this in, but you can if you want to override any painting
    // events for the 2D canvas. For now, we simply call the superclass.

    SupportCanvas2D::paintEvent(e);

}

// ********************************************************************************************
// ** BRUSH
// ********************************************************************************************

void Canvas2D::mouseDown(int x, int y)
{
    // @TODO: [BRUSH] Mouse interaction for brush. You will probably want to create a separate
    //        class for each of your brushes. Remember that you can use the static Settings
    //        object to get the currently selected brush and its parameters.

    int currentBrush = settings.brushType;
    int currentRadius = settings.brushRadius;
    std::cout << "Mouse down " << std::endl;
    BGRA currentColor;
    currentColor.b = settings.brushBlue;
    currentColor.g = settings.brushGreen;
    currentColor.r = settings.brushRed;
    currentColor.a = settings.brushAlpha;


    int currentFlow = settings.brushAlpha;
    // You're going to need to leave the alpha value on the canvas itself at 255, but you will
    // need to use the actual alpha value to compute the new color of the pixel

    bool fixAlphaBlending = settings.fixAlphaBlending; // for extra/half credit

    if (currentBrush == BRUSH_SOLID) {
        m_current_brush = new ConstantBrush(currentColor, currentFlow, currentRadius);
    } else if (currentBrush == BRUSH_LINEAR) {
        m_current_brush = new LinearBrush(currentColor, currentFlow, currentRadius);
    } else if (currentBrush == BRUSH_QUADRATIC) {
        m_current_brush = new QuadraticBrush(currentColor, currentFlow, currentRadius);
    } else if (currentBrush == BRUSH_SMUDGE) {
        m_current_brush = new SmudgeBrush(currentColor, currentFlow, currentRadius);
    } else if (currentBrush == BRUSH_SPECIAL_1) {
        m_current_brush = new SmudgeBrush(currentColor, currentFlow, currentRadius);
    } else {
        m_current_brush = new SmudgeBrush(currentColor, currentFlow, currentRadius);
    }

}

void Canvas2D::mouseDragged(int x, int y)
{
    // TODO: [BRUSH] Mouse interaction for Brush.
    m_current_brush->paintOnce(x, y, this);

}

void Canvas2D::mouseUp(int x, int y)
{
    // TODO: [BRUSH] Mouse interaction for Brush.

}



// ********************************************************************************************
// ** FILTER
// ********************************************************************************************

void Canvas2D::filterImage()
{
    // TODO: [FILTER] Filter the image. Some example code to get the filter type is provided below.
    if (m_filterType != settings.filterType || m_filter == NULL) {
        switch (settings.filterType) {
        case FILTER_BLUR:
            m_filter.reset(new BlurFilter());
            break;
        case FILTER_INVERT:
             m_filter.reset(new InvertFilter());
            break;
        case FILTER_GRAYSCALE:
             m_filter.reset(new GreyscaleFilter());
            break;
        case FILTER_EDGE_DETECT:
             m_filter.reset(new EdgeDetectFilter());
            break;
        case FILTER_SCALE:
             m_filter.reset(new ScaleFilter());
            break;
        case FILTER_ROTATE:
            //m_filter = new RotateFilter();
            break;
        case FILTER_SPECIAL_1:
             m_filter.reset(new MedianFilter());
            break;
        case FILTER_SPECIAL_2:
            //m_filter = new BlurFilter();
            break;
        case FILTER_SPECIAL_3:
            //m_filter = new BlurFilter();
            break;
        }
        m_filterType = settings.filterType;
    }
    m_filter->applyFilter(this, settings);
}

void Canvas2D::setScene(RayScene *scene)
{
    delete m_scene;
    m_scene = scene;
}


void Canvas2D::renderImage(Camera *camera, int width, int height)
{
    if (m_scene)
    {
        // @TODO: raytrace the scene based on settings
        //        YOU MUST FILL THIS IN FOR INTERSECT/RAY

        //std::cout << "W: " << width << ", H: " << height << ", This W: " << this->width() << ", This H: " << this->height() << std::endl;
        this->resize(width, height);
        if (settings.useMultiThreading){
            m_scene->startRender(camera, 8);
        } else {
            m_scene->startRender(camera, 1);
        }
        //m_scene->renderGeometry(width, height, glm::vec2(0,0), glm::vec2(width, height));
        // If you want the interface to stay responsive, make sure to call
        // QCoreApplication::processEvents() periodically during the rendering.

    }
}

void Canvas2D::cancelRender()
{
    m_scene->cancelRender();
}



void Canvas2D::settingsChanged() {

    // TODO: Process changes to the application settings.

}
