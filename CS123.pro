# -------------------------------------------------
# Project created by QtCreator 2010-08-22T14:12:19
# -------------------------------------------------
QT += opengl xml
TARGET = CS123
TEMPLATE = app

unix:!macx {
    LIBS += -lGLU
    QMAKE_CXXFLAGS += -std=c++11
}
macx {
    QMAKE_CFLAGS_X86_64 += -mmacosx-version-min=10.7
    QMAKE_CXXFLAGS_X86_64 = $$QMAKE_CFLAGS_X86_64
}
win32 {
    DEFINES += GLEW_STATIC
    QMAKE_CXXFLAGS += -std=c++11
}

SOURCES += brush/SmudgeBrush.cpp \
    brush/QuadraticBrush.cpp \
    brush/LinearBrush.cpp \
    brush/ConstantBrush.cpp \
    brush/Brush.cpp \
    camera/OrbitingCamera.cpp \
    camera/CamtransCamera.cpp \
    scenegraph/SelectionRecorder.cpp \
    lib/CS123XmlSceneParser.cpp \
    lib/ResourceLoader.cpp \
    scenegraph/ShapesScene.cpp \
    scenegraph/SceneviewScene.cpp \
    scenegraph/Scene.cpp \
    scenegraph/RayScene.cpp \
    scenegraph/OpenGLScene.cpp \
    ui/SupportCanvas3D.cpp \
    ui/SupportCanvas2D.cpp \
    ui/Settings.cpp \
    ui/mainwindow.cpp \
    ui/Databinding.cpp \
    ui/Canvas3D.cpp \
    ui/Canvas2D.cpp \
    main.cpp \
    glew-1.10.0/src/glew.c \
    shapes/cube.cpp \
    shapes/shape.cpp \
    shapes/cylinder.cpp \
    shapes/cone.cpp \
    shapes/sphere.cpp \
    filter/filter.cpp \
    filter/invertfilter.cpp \
    filter/greyscalefilter.cpp \
    filter/edgedetectfilter.cpp \
    filter/scalefilter.cpp \
    filter/blurfilter.cpp \
    filter/medianfilter.cpp \
    scenegraph/RayRunner.cpp \
    scenegraph/ray.cpp \
    scenegraph/collision.cpp \
    scenegraph/ThreeDTree.cpp


HEADERS += brush/SmudgeBrush.h \
    brush/QuadraticBrush.h \
    brush/LinearBrush.h \
    brush/ConstantBrush.h \
    brush/Brush.h \
    camera/OrbitingCamera.h \
    camera/CamtransCamera.h \
    camera/Camera.h \
    scenegraph/SelectionRecorder.h \
    lib/CS123XmlSceneParser.h \
    lib/CS123SceneData.h \
    lib/CS123ISceneParser.h \
    lib/CS123Common.h \
    lib/ResourceLoader.h \
    scenegraph/ShapesScene.h \
    scenegraph/SceneviewScene.h \
    scenegraph/Scene.h \
    scenegraph/RayScene.h \
    scenegraph/OpenGLScene.h \
    ui/SupportCanvas3D.h \
    ui/SupportCanvas2D.h \
    ui/Settings.h \
    ui/mainwindow.h \
    ui/Databinding.h \
    ui/Canvas3D.h \
    ui/Canvas2D.h \
    ui_mainwindow.h \
    glew-1.10.0/include/GL/glew.h \
    shapes/cube.h \
    shapes/shape.h \
    shapes/cylinder.h \
    shapes/cone.h \
    shapes/sphere.h \
    shapes/shape.h \
    filter/filter.h \
    filter/invertfilter.h \
    filter/greyscalefilter.h \
    filter/edgedetectfilter.h \
    filter/scalefilter.h \
    filter/blurfilter.h \
    filter/medianfilter.h \
    scenegraph/RayRunner.h \
    scenegraph/ray.h \
    scenegraph/collision.h \
    scenegraph/ThreeDTree.h


FORMS += ui/mainwindow.ui
INCLUDEPATH += glm brush camera lib scenegraph ui glew-1.10.0/include
DEPENDPATH += glm brush camera lib scenegraph ui glew-1.10.0/include
DEFINES += TIXML_USE_STL
OTHER_FILES += shaders/shader.frag shaders/shader.vert

# Don't add the -pg flag unless you know what you are doing. It makes QThreadPool freeze on Mac OS X
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_CXXFLAGS_WARN_ON -= -Wall
QMAKE_CXXFLAGS_WARN_ON += -Waddress -Warray-bounds -Wc++0x-compat -Wchar-subscripts -Wformat\
                          -Wmain -Wmissing-braces -Wparentheses -Wreorder -Wreturn-type \
                          -Wsequence-point -Wsign-compare -Wstrict-overflow=1 -Wswitch \
                          -Wtrigraphs -Wuninitialized -Wunused-label -Wunused-variable \
                          -Wvolatile-register-var -Wno-extra
# QMAKE_CXX_FLAGS_WARN_ON += -Wunknown-pragmas -Wunused-function -Wmain

macx {
    QMAKE_CXXFLAGS_WARN_ON -= -Warray-bounds -Wc++0x-compat
}

RESOURCES += \
    resources.qrc

DISTFILES += \

