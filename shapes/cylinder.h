#ifndef CYLINDER_H
#define CYLINDER_H
#include "shape.h"


class Cylinder : public Shape
{
public:
    Cylinder(NormalRenderer *normalRenderer, GLuint shader);
    ~Cylinder();
    void updateBuffer(int p1, int p2, int p3);
};

#endif // CYLINDER_H
