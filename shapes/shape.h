#ifndef SHAPE_H
#define SHAPE_H
#include "GL/glew.h"
#include "OpenGLScene.h"
#include <memory>

class Shape
{
public:
    Shape(NormalRenderer *normalRenderer, GLuint shader);
    ~Shape();

    virtual void updateBuffer(int p1, int p2, int p3) = 0;
    virtual void draw();
    int m_bufferSize;
    int m_vertices;
    int m_shapeType;

protected:
    float *m_bufferData;
    NormalRenderer *m_normalRenderer;
    GLuint m_shader;
    GLuint m_vaoID;

    void handleBuffers();
    int addVertex(float* buffer, int counter, float x, float y, float z, glm::vec3 normal, float u, float v);
    int addPlanePerpSquare(float* buffer, int counter, float x1, float y1, float z1, float x2, float y2, float z2, glm::vec3 normal, bool isUp, float u, float v);
    int addTriangle(float *buffer, int counter, float x1, float y1, float z1,
                    float x2, float y2, float z2,
                    float x3, float y3, float z3,
                    glm::vec3 normal1, glm::vec3 normal2, glm::vec3 normal3, bool isUp,
                    float u, float v);
    int round(float f);
};

#endif // SHAPE_H

#include "Brush.h"
