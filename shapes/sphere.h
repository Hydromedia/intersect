#ifndef SPHERE_H
#define SPHERE_H
#include "shape.h"


class Sphere : public Shape
{
public:
    Sphere(NormalRenderer *normalRenderer, GLuint shader);
    ~Sphere();
    void updateBuffer(int p1, int p2, int p3);
};

#endif // SPHERE_H
