#include "cylinder.h"

Cylinder::Cylinder(NormalRenderer *normalRenderer, GLuint shader) :
    Shape(normalRenderer, shader)
{
    m_shapeType = 3;
}

Cylinder::~Cylinder()
{

}

void Cylinder::updateBuffer(int p1, int p2, int p3){
    if (p2 < 3) {
        p2 = 3;
    }
    m_bufferSize = ((p1*2-1)*(p2))*2*3*8 + 2*p1*p2*3*8;
    m_bufferData = new GLfloat[m_bufferSize];
    m_vertices = m_bufferSize/6;
    float r = .5;
    int buffer_counter = 0;
    float iter_amount = (2*M_PI)/((float) p2);
    for (float y = -.5f; y <= .6f; y+=1.f) {
        float x = 0;
        float z = 0;
        for (int p2_iter = 0; p2_iter < p2; p2_iter++) {
            int p1_iter_max = p1;
            for (float p1_iter = 0; p1_iter < p1_iter_max; p1_iter += 1.f) {
                bool up = y > 0 ? true : false;
                float iter_prop = p1_iter / ((float) p1_iter_max);
                float iter_prop2 = (1.f+p1_iter) / ((float) p1_iter_max);
                if (p1_iter != 0) {
                    buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                     (iter_prop)*std::cos(x)*r, y, iter_prop*std::sin(z)*r,
                                                     (iter_prop)*std::cos(x+iter_amount)*r, y, (iter_prop)*std::sin(z+iter_amount)*r,
                                                     (iter_prop2)*std::cos(x)*r, y, (iter_prop2)*std::sin(z)*r,
                                                     glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), up, x, z);

                    buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                     (iter_prop)*std::cos(x+iter_amount)*r, y, (iter_prop)*std::sin(z+iter_amount)*r,
                                                     (iter_prop2)*std::cos(x)*r, y, (iter_prop2)*std::sin(z)*r,
                                                     (iter_prop2)*std::cos(x+iter_amount)*r, y, (iter_prop2)*std::sin(z+iter_amount)*r,
                                                     glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), !up, x, z);
                } else {
                    buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                     (iter_prop)*std::cos(x)*r, y, (iter_prop)*std::sin(z)*r,
                                                     (iter_prop2)*std::cos(x)*r, y, (iter_prop2)*std::sin(z)*r,
                                                     (iter_prop2)*std::cos(x+iter_amount)*r, y, (iter_prop2)*std::sin(z+iter_amount)*r,
                                                     glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), !up, x, z);
                }
            }
            z+=iter_amount;
            x+=iter_amount;
        }
    }

    iter_amount = (2*M_PI)/((float) p2);
    float x = 0;
    float z = 0;
    for (int p2_iter = 0; p2_iter < p2; p2_iter++) {
        int p1_iter_max = p1;
        for (float p1_iter = 0; p1_iter < p1_iter_max; p1_iter += 1.f) {
            float iter_prop = p1_iter / ((float) p1_iter_max);
            float iter_prop2 = (1.f+p1_iter) / ((float) p1_iter_max);
            buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                             std::cos(x)*r, iter_prop-.5f, std::sin(z)*r,
                                             std::cos(x+iter_amount)*r, iter_prop-.5f, std::sin(z+iter_amount)*r,
                                             std::cos(x)*r, iter_prop2-.5f, std::sin(z)*r,
                                             glm::normalize(glm::vec3(std::cos(x)*r, 0, std::sin(z)*r)),
                                             glm::normalize(glm::vec3(std::cos(x+iter_amount)*r, 0, std::sin(z+iter_amount)*r)),
                                             glm::normalize(glm::vec3(std::cos(x)*r, 0, std::sin(z)*r)),
                                             false, x, z);

            buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                 std::cos(x+iter_amount)*r, iter_prop-.5f, std::sin(z+iter_amount)*r,
                                                 std::cos(x)*r, iter_prop2-.5f, std::sin(z)*r,
                                                 std::cos(x+iter_amount)*r, iter_prop2-.5f, std::sin(z+iter_amount)*r,
                                                 glm::normalize(glm::vec3(std::cos(x+iter_amount)*r, 0, std::sin(z+iter_amount)*r)),
                                                 glm::normalize(glm::vec3(std::cos(x)*r, 0, std::sin(z)*r)),
                                                 glm::normalize(glm::vec3(std::cos(x+iter_amount)*r, 0, std::sin(z+iter_amount)*r)),
                                                 true, x, z);
        }
        z+=iter_amount;
        x+=iter_amount;
    }



    handleBuffers();

    // Don't forget to clean up any resources you created
    if (m_bufferData != 0) {
        delete [] m_bufferData;
    }
}


