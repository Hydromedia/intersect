#ifndef CONE_H
#define CONE_H
#include "shape.h"


class Cone : public Shape
{
public:
    Cone(NormalRenderer *normalRenderer, GLuint shader);
    ~Cone();
    void updateBuffer(int p1, int p2, int p3);
};

#endif // CONE_H
